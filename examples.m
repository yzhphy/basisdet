(* ::Package:: *)

(* Make sure the path of the package is correct *)
<<"~/physics/Basis_determination/basisdet/BasisDet.m"


(* D=4 One-loop box *)
(*  Analytic Mode *)
L=1;
Dim=4;
n=4;
ExternalMomentaBasis={p1,p2,p4};
Kinematics={p1^2->0,p2^2->0,p4^2->0,p1 p2->s/2,p1 p4->t/2,p2 p4->-(s+t)/2,\[Omega]1^2->-t(s+t)/s};
numeric={s->11,t->3};
Props={l1,l1-p1,l1-p1-p2,l1+p4};
RenormalizationCondition={{{1},4}};
GenerateBasis[1,MinimalBasis->True] 


(* D=4-2\[Epsilon] One-loop box *)
(*  Analytic Mode *)
L=1;
Dim=4-2\[Epsilon];
n=4;
ExternalMomentaBasis={p1,p2,p4};
Kinematics={p1^2->0,p2^2->0,p4^2->0,p1 p2->s/2,p1 p4->t/2,p2 p4->-(s+t)/2,\[Omega]1^2->-t(s+t)/s};
numeric={s->11,t->3};
Props={l1,l1-p1,l1-p1-p2,l1+p4};
RenormalizationCondition={{{1},4}};
GenerateBasis[1] 
Integrand


(* D=4 One-loop pentagon  *)
(* Analytic Mode *)
(* All terms are reduced in D=4, and the integrand basis should be an empty set. *) 
L=1;
Dim=4;
n=5;
ExternalMomentaBasis={p1,p2,p3,p4};
Kinematics={p1^2->0,p2^2->0,p3^2->0,p4^2->0,p1 p2->s12/2,p1 p3->s13/2,p1 p4->s14/2,p2 p3->s23/2,p2 p4->s24/2,p3 p4->-(s14+s24+s12+s13+s14)/2,\[Omega]1^2->-t(s+t)/s};
numeric={s12->11,s13->3,s14->7,s23->5,s24->13};
Props={l1-p1,l1,l1-p1-p2,l1-p1-p2-p3,l1-p1-p2-p3-p4};
RenormalizationLoopMomenta={{{1},5}};
GenerateBasis[1] 


 (* D=4 One-loop massive box *)
L=1;
Dim=4;
n=4;
sNormalization=-(m1^4 m3^2-m1^2 m2^2 m3^2+m1^2 m3^4-m1^2 m2^2 m4^2+m2^4 m4^2-m1^2 m3^2 m4^2-m2^2 m3^2 m4^2+m2^2 m4^4+m1^2 m2^2 s-m1^2 m3^2 s-m2^2 m4^2 s+m3^2 m4^2 s-m1^2 m3^2 t+m2^2 m3^2 t+m1^2 m4^2 t-m2^2 m4^2 t-m1^2 s t-m2^2 s t-m3^2 s t-m4^2 s t+s^2 t+s t^2)/s^2;
ExternalMomentaBasis={p1,p2,p4};
Kinematics={p1^2->m1^2,p2^2->m2^2,p4^2->m4^2,p1 p2->s/2-m1^2/2-m2^2/2,p1 p4->t/2-m1^2/2-m4^2/2,p2 p4->-(s+t)/2+m1^2/2+m3^2/2,\[Omega]1^2->sNormalization};
numeric={s->11,t->5,m1->1,m2->7,m3->13,m4->23};
Props={l1-p1,l1,l1-p1-p2,l1+p4};
RenormalizationCondition={{{1},4}};
GenerateBasis[1] 
Integrand


 (* D=4 Two-loop double-box *)
(*  Analytic Mode *)
L=2;
Dim=4;
n=4;
ExternalMomentaBasis={p1,p2,p4};
Kinematics={p1^2->0,p2^2->0,p4^2->0,p1 p2->s/2,p1 p4->t/2,p2 p4->-(s+t)/2,\[Omega]1^2->-t(s+t)/s};
numeric={s->11,t->3};
Props={l1-p1,l1,l1-p1-p2,l2-p3-p4,l2,l2-p4,l1+l2};
RenormalizationCondition={{{1,0},4},{{0,1},4},{{1,1},6}};
GenerateBasis[1,MinimalBasis->False] 


 (* D=4 Two-loop double-box *)
(*  Numeric Mode *)
L=2;
Dim=4;
n=4;
ExternalMomentaBasis={p1,p2,p4};
Kinematics={p1^2->0,p2^2->0,p4^2->0,p1 p2->s/2,p1 p4->t/2,p2 p4->-(s+t)/2,\[Omega]1^2->-t(s+t)/s};
numeric={s->11,t->3};
Props={l1-p1,l1,l1-p1-p2,l2-p3-p4,l2,l2-p4,l1+l2};
RenormalizationCondition={{{1,0},4},{{0,1},4},{{1,1},6}};
 GenerateBasis[0,MinimalBasis->False] 
Integrand


 (* D=4-2\[Epsilon] Two-loop double-box *)
(*  Analytic Mode *)
L=2;
Dim=4-2\[Epsilon];
n=4;
ExternalMomentaBasis={p1,p2,p4};
Kinematics={p1^2->0,p2^2->0,p4^2->0,p1 p2->s/2,p1 p4->t/2,p2 p4->-(s+t)/2,\[Omega]1^2->-t(s+t)/s};
numeric={s->11,t->3};
Props={l1-p1,l1,l1-p1-p2,l2-p3-p4,l2,l2-p4,l1+l2};
RenormalizationCondition={{{1,0},4},{{0,1},4},{{1,1},6}};
GenerateBasis[1] 


(* D=4 Two-loop double-box, five-point *)
(*  Numeric Mode *)
L=2;
Dim=4;
n=5;
ExternalMomentaBasis={p1,p2,p3,p4};
Kinematics={p1^2->0,p2^2->0,p3^2->0,p4^2->0,p1 p2->s12/2,p1 p3->s13/2,p1 p4->s14/2,p2 p3->s23/2,p2 p4->s24/2,p3 p4->-(s14+s24+s12+s13+s14)/2,\[Omega]1^2->-t(s+t)/s};
numeric={s12->11,s13->3,s14->7,s23->5,s24->13};
Props={l1-p1,l1,l1-p1-p2,l2-p3-p4,l2,l2-p4,l1+l2};
RenormalizationCondition={{{1,0},4},{{0,1},4},{{1,1},6}};
GenerateBasis[0] 


(* D=4 Two-loop box-triangle *)
(*  Numeric Mode *)
L=2;
Dim=4;
n=3;
ExternalMomentaBasis={p1,p2};
Kinematics={p1^2->0,p2^2->0,p1 p2->m3^2/2,\[Omega]1^2->m3^2/2,\[Omega]2^2->m3^2/2};
numeric={m3->3};
Props={l1-p1,l1,l1-p1-p2,l1+l2,l2,l2-p3};
RenormalizationCondition={{{1,0},4},{{0,1},3},{{1,1},5}};
GenerateBasis[1,MinimalBasis->True] 


(* Sunset *)
L=2;
Dim=4;
n=2;
ExternalMomentaBasis={p1};
Kinematics={p1 p1->m^2,p2 p2->m^2,p1 p2->-m^2,\[Omega]1^2->m^2,\[Omega]2^2->m^2,\[Omega]3^2->m^2};
numeric={m->1};
Props={l1,l2,l1+l2-p2};
RenormalizationCondition={{{1,0},2},{{0,1},2},{{1,1},2}};
GenerateBasis[1,MinimalBasis->True] 


(* D=4 Two-loop pentagon-box *)
(*  Numeric Mode *)
L=2;
Dim=4;
n=5;
ExternalMomentaBasis={p1,p2,p3,p4};
Kinematics={p1^2->0,p2^2->0,p3^2->0,p4^2->0,p1 p2->s12/2,p1 p3->s13/2,p1 p4->s14/2,p2 p3->s23/2,p2 p4->s24/2,p3 p4->-(s14+s24+s12+s13+s14)/2,\[Omega]1^2->-t(s+t)/s,\[Omega]2^2->-t(s+t)/s};
numeric={s12->11,s13->3,s14->7,s23->5,s24->13};
Props={l1,l1-p1,l1-p1-p2,l1-p1-p2-p3,l2,l2+p1+p2+p3+p4,l2+p1+p2+p3,l1+l2};
RenormalizationCondition={{{1,0},5},{{0,1},4},{{1,1},7}};
GenerateBasis[0] 


(* D=4 Two-loop non-planer double triangle*)
(*  Numeric Mode *)
L=2;
Dim=4;
n=5;
ExternalMomentaBasis={p1,p2,p3,p4};
Kinematics={p1^2->0,p2^2->0,p3^2->0,p4^2->0,p1 p2->s12/2,p1 p3->s13/2,p1 p4->s14/2,p2 p3->s23/2,p2 p4->s24/2,p3 p4->-(s14+s24+s12+s13+s14)/2};
numeric={s12->11,s13->3,s14->7,s23->5,s24->13};
Props={l1,l1-p1,l2,l2-p3,l1+l2-p1-p2-p3,l1+l2+p4};
RenormalizationCondition={{{1,0},4},{{0,1},4},{{1,1},5}};
GenerateBasis[0,MinimalBasis->True] 


(* D=4 Two-loop pentagon-box *)
(*  Numeric Mode *)
L=2;
Dim=4;
n=5;
ExternalMomentaBasis={p1,p2,p3,p4};
Kinematics={p1^2->0,p2^2->0,p3^2->0,p4^2->0,p5^2->0,p1 p2->s12/2,p1 p3->s13/2,p1 p4->s14/2,p1 p5->s15/2,p2 p3->s23/2,p2 p4->s24/2,p2 p5->s25/2,p3 p4->s34/2,p3 p5->s35/2,p4 p5->s45/2};
numeric={s12->11,s13->3,s14->7,s23->5,s24->13,s15->-21,s25->-29,s34->-39,s35->31,s45->19};
Props={l2,l2-p3,l2-p3-p4,l2-p3-p4-p5,l1,l1-p2,l1-p1-p2,l1+l2};
RenormalizationCondition={{{1,0},4},{{0,1},5},{{1,1},7}};
GenerateBasis[0] 


(* D=4 Three-loop triple-box *)
(*  Numeric Mode *)
Dim=4; 
L=3;
n=4;
ExternalMomentaBasis={p1,p2,p4};
Kinematics={p1^2->0,p2^2->0,p4^2->0,p1 p2->s/2,p1 p4->t/2,p2 p4->-(s+t)/2,\[Omega]1^2->-t(s+t)/s};
numeric={s->11,t->3};
Props={l1-p1,l1,l1-p1-p2,l2-p3-p4,l2,l2-p4,l1+l3,l2-l3,l3+p1+p2,l3};
RenormalizationCondition={{{1,0,0},4},{{0,1,0},4},{{0,0,1},4},{{1,1,1},8},{{1,0,1},6},{{0,1,1},6}};
GenerateBasis[0] 


(* D=4 Four-loop quadruple-box *)
(*  Numeric Mode *)
(* It would take about 3 hours. *)
(* Dim=4;
L=4;
n=4;
ExternalMomentaBasis={p1,p2,p4};
Kinematics={p1^2->0,p2^2->0,p4^2->0,p1 p2->s/2,p1 p4->t/2,p2 p4->-(s+t)/2,\[Omega]1^2->-t (s+t)/s};
numeric={s->11,t->3};
Props={l1,l1-p1,l1-p1-p2,l1-l3,l3-p1-p2,l3-l4,l3,l4-p1-p2,l4-l2,l4,l2-p1-p2,l2+p4,l2};
RenormalizationCondition={{{1,0,0,0},4},{{0,1,0,0},4},{{0,0,1,0},4},{{0,0,0,1},4},{{1,0,1,0},6},{{0,0,1,1},6},{{0,1,0,1},6},{{1,0,1,1},8},{{0,1,1,1},8},{{1,1,1,1},10}};
GenerateBasis[0] *)
